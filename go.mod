module gingorm

go 1.13

require (
	github.com/appleboy/gin-jwt/v2 v2.6.3
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.7.4
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/gofrs/uuid v4.0.0+incompatible
	github.com/google/go-cmp v0.5.6 // indirect
	github.com/google/uuid v1.3.0
	github.com/joho/godotenv v1.4.0
	github.com/lib/pq v1.10.3 // indirect
	github.com/onsi/gomega v1.16.0 // indirect
	github.com/swaggo/gin-swagger v1.3.2
	github.com/swaggo/swag v1.7.3
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519
	gorm.io/driver/postgres v1.1.2
	gorm.io/gorm v1.21.16
)
